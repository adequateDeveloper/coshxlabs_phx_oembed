defmodule PhxOembed.CardTest do
  use PhxOembed.ModelCase

  alias PhxOembed.Card

  @valid_attrs %{author_name: "some content", author_url: "some content", cache_age: "some content", card_type: "some content", provider_name: "some content", provider_url: "some content", thumbnail_height: "some content", thumbnail_url: "some content", thumbnail_width: "some content", title: "some content", url: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Card.changeset(%Card{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Card.changeset(%Card{}, @invalid_attrs)
    refute changeset.valid?
  end
end
