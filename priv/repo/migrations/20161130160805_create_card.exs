defmodule PhxOembed.Repo.Migrations.CreateCard do
  use Ecto.Migration

  def change do
    create table(:cards) do
      add :url, :string, null: false
      add :card_type, :string, null: false
      add :title, :string
      add :author_name, :string
      add :author_url, :string
      add :provider_name, :string
      add :provider_url, :string
      add :cache_age, :string
      add :thumbnail_url, :string
      add :thumbnail_width, :string
      add :thumbnail_height, :string

      timestamps()
    end

  end
end
