
https://www.coshx.com/blog/2016/03/16/json-api-with-phoenix/

https://github.com/coshx/phoenix_tutorial
http://oembed.com/
------------------------------------------------------------------------------------------------------------------------
What is oEmbed?

oEmbed is protocol specifying how servers can provide JSON meta data about web pages.

In other words, when people share your links on things like Twitter, Facebook, and Slack, those services will look for
an oEmbed server and ask it for the snippet information, instead of trying to parse your page for meta tags.
It’s pretty simple – you take a url as a query string parameter, and return some json specific to that url
(author information, description, etc). You tell the world about your oEmbed server by adding a tag in your site’s
<head> element.
------------------------------------------------------------------------------------------------------------------------

Created app skeleton using: '$ mix phoenix.new phx_oembed --no-brunch --no-html'

$ 'cd phx_oembed'

Created postgresql tables: '$ mix ecto.create'

To scaffold a JSON resource: '$ mix phoenix.gen.json Card cards url card_type title author_name author_url
                              provider_name provider_url cache_age thumbnail_url thumbnail_width thumbnail_height'

Add a resource line to /web/router.ex
Run: '$ mix phoenix.routes'
Run: '$ mix ecto.migrate'
------------------------------------------------------------------------------------------------------------------------
