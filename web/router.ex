defmodule PhxOembed.Router do
  use PhxOembed.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", PhxOembed do
    pipe_through :api

    resources "/cards", CardController, except: [:new, :edit]
  end
end
